package br.com.hackaton.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicoDeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicoDeApiApplication.class, args);
	}

}
