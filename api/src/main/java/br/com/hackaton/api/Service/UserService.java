package br.com.hackaton.api.Service;

import br.com.hackaton.api.Entity.UserEntity;
import br.com.hackaton.api.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    UserRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public UserEntity salvar(UserEntity user){
        try{

            return repository.save(user);
        }catch (Exception e){
            throw new RuntimeException();
        }
    }
}
