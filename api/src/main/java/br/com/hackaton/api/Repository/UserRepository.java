package br.com.hackaton.api.Repository;

import br.com.hackaton.api.Entity.Enum.TipoUsuarioEnum;
import br.com.hackaton.api.Entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity,Integer> {

    UserEntity findByLogin(String login);
    UserEntity findBySenha(String senha);
    UserEntity findByTipoUsuario(TipoUsuarioEnum tipoUsuario);
}
