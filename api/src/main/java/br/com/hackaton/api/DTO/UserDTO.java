package br.com.hackaton.api.DTO;

import br.com.hackaton.api.Entity.Enum.TipoUsuarioEnum;
import br.com.hackaton.api.Entity.RoleEntity;
import br.com.hackaton.api.Entity.UserEntity;

import javax.persistence.Id;
import java.util.List;

public class UserDTO {

    @Id
    private String login;
    private String senha;
    private TipoUsuarioEnum tipoUsuario;
    private List<RoleEntity> roles;

    public UserDTO(){}

    public UserDTO(UserEntity users){
        this.login = users.getLogin();
        this.senha = users.getSenha();
        this.tipoUsuario = users.getTipoUsuario();
        this.roles = users.getRoles();
    }

    public UserEntity convert(){
        UserEntity users = new UserEntity();
        users.setLogin(this.login);
        users.setSenha(this.senha);
        users.setTipoUsuario(this.tipoUsuario);
        users.setRoles(this.roles);

        return users;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public TipoUsuarioEnum getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleEntity> roles) {
        this.roles = roles;
    }
}
