package br.com.hackaton.api.Config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ConfigurationCors implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry){ //Externo
        registry
                .addMapping("/**")
                .allowedMethods("*")
                .exposedHeaders("Authorization");
    }
}
