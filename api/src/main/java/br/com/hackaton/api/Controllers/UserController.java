package br.com.hackaton.api.Controllers;

import br.com.hackaton.api.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/chamar")
    @ResponseBody
    public String chamadaDados(){
        return "teste";
    }

    @GetMapping(value = "/consultar")
    @ResponseBody
    public String consultarDados(){
        return "teste";
    }



}
