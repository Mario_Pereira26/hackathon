package br.com.dbccompany.hackathon.leitorDados.Config;

import br.com.dbccompany.hackathon.leitorDados.Entity.ArquivoDatEntity;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job job(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory,
                   ItemReader<ArquivoDatEntity> itemReader,
                   ItemProcessor<ArquivoDatEntity, ArquivoDatEntity> itemProcessor) throws Exception {

        Step step = stepBuilderFactory.get("ETL-file-load")
                .<ArquivoDatEntity, ArquivoDatEntity>chunk(1)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(writer())
                .build();

        return jobBuilderFactory.get("ETL-Load")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    @Bean
    public FlatFileItemReader<ArquivoDatEntity> itemReader() {
        FlatFileItemReader<ArquivoDatEntity> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new FileSystemResource("src/main/resources/data/in/arquivoTest.done.dat"));
        flatFileItemReader.setName("DAT-Reader");
        flatFileItemReader.setLineMapper(linerMapper());

        return flatFileItemReader;
    }

    @Bean
    public LineMapper<ArquivoDatEntity> linerMapper() {

        DefaultLineMapper<ArquivoDatEntity> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter("ç");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames(new String[]{"parametroUm","parametroDois","parametroTres","parametroQuatro"});// config para vendedor

        BeanWrapperFieldSetMapper<ArquivoDatEntity> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(ArquivoDatEntity.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }

    public FlatFileItemWriter writer() throws Exception {

        FlatFileItemWriter<ArquivoDatEntity> dadosWriter = new FlatFileItemWriter<>();
        dadosWriter.setResource(new FileSystemResource("src/main/resources/data/out/flat_file_out.done.dat"));
        dadosWriter.setAppendAllowed(true);
        dadosWriter.setLineAggregator(new DelimitedLineAggregator<ArquivoDatEntity>(){
            {
                setDelimiter("\n");
                setFieldExtractor(new BeanWrapperFieldExtractor<ArquivoDatEntity>(){
                    {
                        setNames(new String[]{
                                "parametroUm", "parametroDois", "parametroTres", "parametroQuatro"
                        });
                    }
                });
            }
        });
        return dadosWriter;

    }
}
