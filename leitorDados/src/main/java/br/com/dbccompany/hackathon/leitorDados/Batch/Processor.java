package br.com.dbccompany.hackathon.leitorDados.Batch;

import br.com.dbccompany.hackathon.leitorDados.Entity.ArquivoDatEntity;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class Processor implements ItemProcessor<ArquivoDatEntity, ArquivoDatEntity> {

    private int countClientes;
    private int countVendedores;
    private String nomePiorVendedor;
    private double menorVenda;
    private double maiorVenda;
    private String idVendaMaisCara;

    {
        this.countClientes   = 0;
        this.countVendedores = 0;
        this.maiorVenda = 0;
        this.nomePiorVendedor = "";
        this.idVendaMaisCara = "";
    }

    @Override
    public ArquivoDatEntity process(ArquivoDatEntity dado) throws Exception {

        if(dado.getParametroUm().equals("001")) {
            countVendedores++;
        }
        if(dado.getParametroUm().equals("002")){
            countClientes++;
        }
        if(dado.getParametroUm().equals("003")){
            String itens = dado.getParametroTres().replaceAll("\\[","");
            itens = itens.replaceAll("\\]", "");
            String[] arrayItens = itens.split(",");

            double valorTotalVenda = 0;
            for(String item : arrayItens){
                String[] itensSeparados = item.split("-");
                valorTotalVenda += ( Double.parseDouble( itensSeparados[1] ) * Double.parseDouble( itensSeparados[2] ) );
            }
            if(maiorVenda < valorTotalVenda){
                maiorVenda = valorTotalVenda;
                idVendaMaisCara = dado.getParametroDois();
            }
            if(menorVenda > valorTotalVenda || menorVenda == 0){
                menorVenda = valorTotalVenda;
                nomePiorVendedor = dado.getParametroQuatro();
            }
        }

        dado.setParametroUm("Quantidade de Clientes: " + countClientes);
        dado.setParametroDois("Quantidade de Vendedores: " + countVendedores);
        dado.setParametroTres("Id melhor venda: " + idVendaMaisCara);
        dado.setParametroQuatro("Nome do pior vendedor: " + nomePiorVendedor);

        if(countClientes > 0 && countVendedores > 0 &&
            idVendaMaisCara.length() > 0 && nomePiorVendedor.length() > 0) {
            return dado;
        }
        return null;
    }
}
