package br.com.dbccompany.hackathon.leitorDados.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ArquivoDatEntity {

    @Id
    private String parametroUm;
    private String parametroDois;
    private String parametroTres;
    private String parametroQuatro;

    public ArquivoDatEntity() {}

    public ArquivoDatEntity(String parametroUm, String parametroDois, String parametroTres, String parametroQuatro) {
        this.parametroUm = parametroUm;
        this.parametroDois = parametroDois;
        this.parametroTres = parametroTres;
        this.parametroQuatro = parametroQuatro;
    }

    public String getParametroUm() {
        return parametroUm;
    }

    public void setParametroUm(String parametroUm) {
        this.parametroUm = parametroUm;
    }

    public String getParametroDois() {
        return parametroDois;
    }

    public void setParametroDois(String parametroDois) {
        this.parametroDois = parametroDois;
    }

    public String getParametroTres() {
        return parametroTres;
    }

    public void setParametroTres(String parametroTres) {
        this.parametroTres = parametroTres;
    }

    public String getParametroQuatro() {
        return parametroQuatro;
    }

    public void setParametroQuatro(String parametroQuatro) {
        this.parametroQuatro = parametroQuatro;
    }

    @Override
    public String toString() {
        return "ArquivoDatEntity{" +
                "parametroUm='" + parametroUm + '\'' +
                ", parametroDois='" + parametroDois + '\'' +
                ", parametroTres='" + parametroTres + '\'' +
                ", parametroQuatro='" + parametroQuatro + '\'' +
                '}';
    }
}
