
## Hackathon
Este projeto destina-se a criar dois serviços, sendo um deles para leitura de dados com ETL e outro sendo uma API Rest.

1. Serviço leitura de dados;
2. Serviço API;

---
## Serviço leitura de dados:

Para o serviço de leitura de dados utilizamos do framework Spring Batch pertencente ao ecossistema Spring. Ele visa fornecer funções reutilizáveis para o processamento e gerenciamento de dados em um determinado cenário de integração de sistemas através de aplicações batch.

Para este projeto os dados para processamento são oriundos de arquivos .dat e após serem tratados retornam em formato de relatório em um arquivo .dat também.

## Serviço API:

API consome os arquivos .dat gerados pelo serviço de leitura de dados, e os expõe para visualização web.

## Tecnologias utilizadas:

Spring boot
Spring batch
Docker

Ferramentas utilizadas 
IntelliJ
Postman


## Autores:
Gilberto Junior
Gustavo Gallarreta
Mario Henrique Pereira da Rosa